# Rust Tokens Coding Challenge

This coding challenge presents a tokenization/parsing scenario in which some
external system is accepting bytes in discrete packets representing a stream.

You will solve the challenge by modifying the method `process` of the struct `Parser`, without changing its function signature or the signature of the constructor, `new`. You can, however, modify the struct `Parser` itself however you feel is necessary.

## Packetization
These packets aren't consistent in their size or contents, so the stream
`"I am a stream of packets; synthesize me."` could just as easily be
`"I am ", "a stream ", "of packets; ", "synthesize me."`,
one packet per character (`"I", " ", "a", "m", ...`), or just one packet
containing the entire input.

## Input and Output

Your task is to implement a struct, called `Parser`, whose `process` method
adds a suffix after certain words. So, for instance, if given the token `"foo"` and the suffix `"bar"`, your `Parser` would take the following input:

- `"Does this foo look like a fooing bar to you?"`

and return this output:

- `"Does this foobar look like a foobaring bar to you?"`

This needs to work over the full range of possible packetizations,
from one packet for the whole input to one packet per character.

## Mechanics of I/O

The `process` function takes a mutable reference to the `Parser` (so you can 
add some state into the `Parser` if you want),
a chunk of bytes to process,
and a `&mut dyn Write` sink for the output.
If you're not familiar with the `Write` trait, it's worth looking into.
The gist, however, is that you can send a buffer (like a `&[u8]`) to the
`Write`r with its `write_all` method.

This is actually what the default implementation of `process` does, as provided by the challenge, which is of course not a correct solution - but it does pass half of the tests!

## Non-functional Requirements

There are no constraints on speed, memory usage, or binary size. Go wild.

# This Repo

This Git repo has just two commits, on the branches `challenge` (and `main`) and `solution`.

Don't look at `solution` until you've solved the challenge!